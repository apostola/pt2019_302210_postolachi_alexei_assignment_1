package demo;

import static org.junit.Assert.*;
import org.junit.*;
import org.junit.internal.Throwables;

import model.Model;

public class JUnitTest {
	private static Model model;
	private static int nrTesteExecutate = 0;
	private static int nrTesteCuSucces = 0;
	
	public JUnitTest() {System.out.println("Inaintea testului: "+nrTesteExecutate);}
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {model = new Model();}
	@AfterClass
	public static void tearDownAfterClass() throws Exception{
		System.out.println("S-au executat: "+nrTesteExecutate+
				" Nr. teste cu succe: "+nrTesteCuSucces);
	}
	@Before
	public void setUp() throws Exception {nrTesteExecutate++;}
	@After
	public void tearDown() throws Exception {System.out.println("S-a terminat testul curent: "+nrTesteExecutate);}
	
	@Test
	public void testReadPolynom() {
		model.reset();
		model.readPolinom("X^2", true);
		model.readPolinom("X^3", false);
		String pol = model.returnPolinom();
		String pol2 = model.returnPolinom2();
		assertNotNull(pol);
		assertNotNull(pol2);
		assertEquals(pol, "X^2");
		assertEquals(pol2, "X^3");
		
		nrTesteCuSucces++;
	}
	@Test
	public void testAddWithSecondArgument() {
		model.reset();
		model.readPolinom("X^2", true);
		model.readPolinom("X^3+5", false);
		model.add();
		String rez = model.returnRezultat();
		assertNotNull(rez);
		assertEquals(rez, "X^3+X^2+5.0");

		nrTesteCuSucces++;
	}
	@Test
	public void testAddNoSecondArgument() {
		model.reset();
		model.readPolinom("X^2", true);
		model.readPolinom("", false);
		model.add();
		String rez = model.returnRezultat();
		assertNotNull(rez);
		assertEquals(rez, "X^2");
		
		nrTesteCuSucces++;
	}
	@Test
	public void testSubWithSecondArgument() {
		model.reset();
		model.readPolinom("X^2", true);
		model.readPolinom("X^2+5", false);
		model.sub();
		String rez = model.returnRezultat();
		assertNotNull(rez);
		assertEquals(rez, "-5.0");

		nrTesteCuSucces++;
	}
	@Test
	public void testSubNoSecondArgument() {
		model.reset();
		model.readPolinom("X^2", true);
		model.readPolinom("", false);
		model.sub();
		String rez = model.returnRezultat();
		assertNotNull(rez);
		assertEquals(rez, "X^2");

		nrTesteCuSucces++;
	}
	@Test
	public void testMulTwoPolynomials() {
		model.reset();
		model.readPolinom("X^2", true);
		model.readPolinom("X", false);
		model.mul();
		String rez = model.returnRezultat();
		assertNotNull(rez);
		assertEquals(rez, "X^3");
		
		nrTesteCuSucces++;
	}
	@Test
	public void testMulNoSecondPolynomials() {
		model.reset();
		model.readPolinom("X^2", true);
		model.mul();
		String rez = model.returnRezultat();
		assertNotNull(rez);
		assertEquals(rez, "0");
		
		nrTesteCuSucces++;
	}
	@Test
	public void testDifPolynomial() {
		model.reset();
		model.readPolinom("X^2", true);
		model.der();
		String rez = model.returnRezultat();
		assertNotNull(rez);
		assertEquals(rez, "2.0X");
		
		nrTesteCuSucces++;
	}
	@Test
	public void testIntPolynomial() {
		model.reset();
		model.readPolinom("X^2+5", true);
		model.integ();
		String rez = model.returnRezultat();
		assertNotNull(rez);
		assertEquals(rez, "0.3333333333333333X^3+5.0X");
		
		nrTesteExecutate++;
	}
}
