package demo;

import polynomial.*;
import vizual.Controler;
import vizual.View;
import model.*;
import javax.swing.*;

public class Main {

	public static void main(String[] args) {
		
		Model model = new Model();
		View view = new View();
		@SuppressWarnings("unused")
		Controler controller = new Controler(view, model);
		view.setVisible(true);
	}

}
