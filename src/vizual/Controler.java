package vizual;
import java.awt.event.*;

import model.Model;

public class Controler {
	private View view;
	private Model model;
	
	public Controler(View v, Model m) {
		view = v;
		model = m;
		
		view.addSubListener(new SubListener());
		view.addAddListener(new AddListener());
		view.addMulListener(new MulListener());
		view.addDerListener(new DerListener());
		view.addIntListener(new IntListener());
		view.addDivListener(new DivListener());
		view.addResetListener(new ResetListener());
	}
	
	class AddListener implements ActionListener {
		public void actionPerformed(ActionEvent clicked) {
			String polinom1;
			String polinom2;
			try {
				polinom1 = view.GetUsetInput(true);
				polinom2 = view.GetUsetInput(false);
				
				model.readPolinom(polinom1, true);
				model.readPolinom(polinom2, false);
				model.add();
				view.SetRezult(model.returnRezultat());
			} catch (Exception e) {
				view.showError("Datele introduse nu pot fi procesate, ati introdus un polinom gresit");
			}
			
		}	
	}
	class SubListener implements ActionListener {
		public void actionPerformed(ActionEvent clicked) {
			String polinom1;
			String polinom2;
			try {
				polinom1 = view.GetUsetInput(true);
				polinom2 = view.GetUsetInput(false);
				
				model.readPolinom(polinom1, true);
				model.readPolinom(polinom2, false);
				model.sub();
				view.SetRezult(model.returnRezultat());
			} catch (Exception e) {
				view.showError("Datele introduse nu pot fi procesate, ati introdus un polinom gresit");
			}
			
		}	
	}
	class MulListener implements ActionListener {
		public void actionPerformed(ActionEvent clicked) {
			String polinom1;
			String polinom2;
			try {
				polinom1 = view.GetUsetInput(true);
				polinom2 = view.GetUsetInput(false);
				
				model.readPolinom(polinom1, true);
				model.readPolinom(polinom2, false);
				model.mul();
				view.SetRezult(model.returnRezultat());
			} catch (Exception e) {
				view.showError("Datele introduse nu pot fi procesate, ati introdus un polinom gresit");
			}
			
		}	
	}
	class DivListener implements ActionListener {
		public void actionPerformed(ActionEvent clicked) {
			String polinom1;
			String polinom2;
			try {
//				polinom1 = view.GetUsetInput(true);
//				polinom2 = view.GetUsetInput(false);
				
//				model.readPolinom(polinom1, true);
//				model.readPolinom(polinom2, false);
//				model.div();
				view.showError("Buy full version to perform the operation");
			} catch (Exception e) {
				view.showError("Datele introduse nu pot fi procesate, ati introdus un polinom gresit");
			}
		}	
	}
	class DerListener implements ActionListener {
		public void actionPerformed(ActionEvent clicked) {
			String polinom1;
			try {
				polinom1 = view.GetUsetInput(true);
				
				model.readPolinom(polinom1, true);
				model.der();
				view.SetRezult(model.returnRezultat());
			} catch (Exception e) {
				view.showError("Datele introduse nu pot fi procesate, ati introdus un polinom gresit");
			}
			
		}	
	}
	class IntListener implements ActionListener {
		public void actionPerformed(ActionEvent clicked) {
			String polinom1;
			try {
				polinom1 = view.GetUsetInput(true);
				
				model.readPolinom(polinom1, true);
				model.integ();
				view.SetRezult(model.returnRezultat() + "+C");
			} catch (Exception e) {
				view.showError("Datele introduse nu pot fi procesate, ati introdus un polinom gresit");
			}
			
		}	
	}
	class ResetListener implements ActionListener {
		public void actionPerformed(ActionEvent clicked) {
			try {
				view.reset();
				model.reset();
			} catch (Exception e) {
				view.showError("Impossible to reset");
			}
		}
	}
}
