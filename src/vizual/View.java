package vizual;
import java.awt.*;
import javax.swing.*;

import model.Model;

import java.awt.event.*;

public class View extends JFrame{
	private JTextField userInputPol1 = new JTextField(50);
	private JTextField userInputPol2 = new JTextField(50);
	private JTextField userOutputPolrez = new JTextField(50);
	private JButton addBtn = new JButton("Add");
	private JButton mulBtn = new JButton("Mul");
	private JButton subBtn = new JButton("Subs");
	private JButton derBtn = new JButton("Derive");
	private JButton intBtn = new JButton("Integrate");
	private JButton divBtn = new JButton("Divide");
	private JButton resetBtn = new JButton("Reset");
	
	public View() {
		userInputPol1.setBackground(new Color(64, 64, 65));
		userInputPol1.setForeground(new Color(255, 255, 255));
		userInputPol2.setBackground(new Color(64, 64, 65));
		userInputPol2.setForeground(new Color(255, 255, 255));
		userOutputPolrez.setBackground(new Color(64, 64, 65));
		userOutputPolrez.setForeground(new Color(255, 255, 255));
		addBtn.setBackground(new Color(26, 171, 91));
		addBtn.setForeground(new Color(255, 255, 255));
		subBtn.setBackground(new Color(26, 171, 91));
		subBtn.setForeground(new Color(255, 255, 255));
		mulBtn.setBackground(new Color(26, 171, 91));
		mulBtn.setForeground(new Color(255, 255, 255));
		divBtn.setBackground(new Color(26, 171, 91));
		divBtn.setForeground(new Color(255, 255, 255));
		derBtn.setBackground(new Color(26, 171, 91));
		derBtn.setForeground(new Color(255, 255, 255));
		intBtn.setBackground(new Color(26, 171, 91));
		intBtn.setForeground(new Color(255, 255, 255));
		resetBtn.setBackground(new Color(26, 171, 91));
		resetBtn.setForeground(new Color(255, 255, 255));	
		JPanel content = new JPanel();
		content.setLayout(new GridLayout(5, 1, 20, 20));
		content.add(userInputPol1);
		content.add(userInputPol2);
		JPanel btnBox = new JPanel();
		btnBox.setBackground(new Color(64, 64, 65));
		btnBox.setLayout(new FlowLayout());
		btnBox.add(addBtn);
		btnBox.add(subBtn);
		btnBox.add(mulBtn);
		btnBox.add(divBtn);
		btnBox.add(derBtn);
		btnBox.add(intBtn);
		content.add(btnBox);
		content.add(userOutputPolrez);
		content.add(resetBtn);
		
		
		this.setContentPane(content);
		this.getContentPane().setBackground(new Color(64, 64, 65));
		this.pack();
		
		this.setTitle("Simple Polynomial Calculator");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	void reset() {
		userInputPol1.setText("");
		userInputPol2.setText("");
		userOutputPolrez.setText("");
	}
	
	String GetUsetInput(boolean pol) {return (pol) ? userInputPol1.getText() : userInputPol2.getText();}
	void SetRezult(String polynom) {userOutputPolrez.setText(polynom);}
	void showError(String errMessage) {JOptionPane.showMessageDialog(this, errMessage);}
	void addAddListener(ActionListener onAdd) {addBtn.addActionListener(onAdd);}
	void addSubListener(ActionListener onSub) {subBtn.addActionListener(onSub);}
	void addMulListener(ActionListener onMul) {mulBtn.addActionListener(onMul);}
	void addDivListener(ActionListener onDiv) {divBtn.addActionListener(onDiv);}
	void addDerListener(ActionListener onDer) {derBtn.addActionListener(onDer);}
	void addIntListener(ActionListener onInt) {intBtn.addActionListener(onInt);}
	void addResetListener(ActionListener onReset) {resetBtn.addActionListener(onReset);}
}
