package polynomial;

public class Operatii {
//	Polinom polyOne = new Polinom();
//	Polinom polyTwo = new Polinom();
	private static Polinom polyRez = new Polinom();
	private static Polinom polyRem = new Polinom();
	
	public Operatii() {
		
	}
	
	public static Polinom add(Polinom polyOne, Polinom polyTwo) {
		polyRez = new Polinom();
		int maxGrad = (polyOne.getPower() > polyTwo.getPower()) ? polyOne.getPower() : polyTwo.getPower();
		Monom mon1 = (polyOne.getMonom(maxGrad) != null) ? polyOne.getMonom(maxGrad) : null;
		Monom mon2 = (polyTwo.getMonom(maxGrad) != null) ? polyTwo.getMonom(maxGrad) : null;
		Monom monRez = null;
		while(maxGrad >= 0) {
			if (mon1 != null && mon2 != null) {
				monRez = new Monom(mon1);
				monRez.add(mon2);
				polyRez.insert(monRez);
			} else if (mon1 != null) {
				monRez = new Monom(mon1);
				polyRez.insert(monRez);
			} else if(mon2 != null) {
				monRez = new Monom(mon2); 
				polyRez.insert(monRez);
			}
			maxGrad--;
			mon1 = (polyOne.getMonom(maxGrad) != null) ? new Monom(polyOne.getMonom(maxGrad)) : null;
			mon2 = (polyTwo.getMonom(maxGrad) != null) ? new Monom(polyTwo.getMonom(maxGrad)) : null;
		}
		return polyRez;
	}
	public static Polinom sub(Polinom polyOne, Polinom polyTwo) {
		polyRez = new Polinom();
		int maxGrad = (polyOne.getPower() > polyTwo.getPower()) ? polyOne.getPower() : polyTwo.getPower();
		Monom mon1 = (polyOne.getMonom(maxGrad) != null) ? new Monom(polyOne.getMonom(maxGrad)) : null;
		Monom mon2 = polyTwo.getMonom(maxGrad);
		Monom monRez = null;
		while (maxGrad >= 0) {
			monRez = new Monom(mon1);
			if (mon1 != null && mon2 != null) {monRez.sub(mon2);}
			else if (mon2 != null) {
				monRez = new Monom(mon2);
				monRez.inv(); 
			}
			System.out.println(monRez);
			if (monRez != null && monRez.getval() != 0) polyRez.insert(monRez);
			maxGrad--;
			mon1 = (polyOne.getMonom(maxGrad) != null) ? new Monom(polyOne.getMonom(maxGrad)) : null;
			mon2 = polyTwo.getMonom(maxGrad);
		}
		return polyRez;
	}
	public static Polinom derive(Polinom polyOne) {
		polyRez = new Polinom();
		int maxGrad = polyOne.getPower();
		Monom mon1 = (polyOne.getMonom(maxGrad) != null) ? new Monom(polyOne.getMonom(maxGrad)) : null;
		while(maxGrad >= 1) {
			if (mon1 != null) {
				mon1.derive();
				polyRez.insert(mon1);
			}
			maxGrad--;
			mon1 = (polyOne.getMonom(maxGrad) != null) ? new Monom(polyOne.getMonom(maxGrad)) : null;
		}
		return polyRez;
	}
	public static Polinom integrate(Polinom polyOne) {
		int maxGrad = polyOne.getPower();
		polyRez = new Polinom();
		Monom mon1 = (polyOne.getMonom(maxGrad) != null) ? new Monom(polyOne.getMonom(maxGrad)) : null;
		while(maxGrad >= 0) {
			if (mon1 != null) {
				mon1.integrate();
				polyRez.insert(mon1);
			}
			maxGrad--;
			mon1 = (polyOne.getMonom(maxGrad) != null) ? new Monom(polyOne.getMonom(maxGrad)) : null;
		}
		return polyRez;
	}
	public static Polinom mult(Polinom polyOne, Polinom polyTwo) {
		int iterator = 0;
		int maxGradP1 = polyOne.getPower();
		int maxGradP2 = polyTwo.getPower();
		polyRez = new Polinom();
		Monom mon1 = (polyOne.getMonom(maxGradP1) != null) ? new Monom(polyOne.getMonom(maxGradP1)) : null;
		Monom mon2 = (polyTwo.getMonom(maxGradP2) != null) ? new Monom(polyTwo.getMonom(maxGradP2)) : null;
		Monom monRez;
		while (maxGradP2 >= 0) {
			iterator = maxGradP1;
			while (iterator >= 0) {
				if (mon1 != null && mon2 != null) {
					monRez = new Monom(mon1);
					monRez.mul(mon2);
					polyRez.insert(monRez);
				}
				iterator--;
				mon1 = (polyOne.getMonom(iterator) != null) ? new Monom(polyOne.getMonom(iterator)) : null;
			}
			maxGradP2--;
			mon2 = (polyTwo.getMonom(maxGradP2) != null) ? new Monom(polyTwo.getMonom(maxGradP2)) : null;
		}
		return polyRez;
	}
//	public boolean div() {
//		int gradPolyRez;
//		int gradPolyRem;
//		polyRez = new Polinom();
//		polyRem = new Polinom();
//		
//		if (polyTwo.isZero()) {return false;}
//		Polinom polyLead = new Polinom(polyTwo.getMonom(polyTwo.getPower())); // cream un polinom din monomul cu puterea cea mai mare a impartitorului
//		polyRem = (!polyOne.isZero()) ? new Polinom(polyOne) : null;
//		gradPolyRem = polyRem.getPower();
//		
//		while (!polyRem.isZero() && gradPolyRem >= polyTwo.getPower()) {
//			
//		}
//		return true;
//	}
	
}
