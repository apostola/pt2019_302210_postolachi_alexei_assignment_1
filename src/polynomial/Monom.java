package polynomial;

public class Monom {
	private int grad;
	private double valoare;
	
	public Monom() {
		grad = 0;
		valoare = 0;
	}
	
	public Monom(int grad) {
		this.grad = grad;
		valoare = 0;
	}
	
	public Monom(int grad, double valoare) {
		this.grad = grad;
		this.valoare = valoare;
	}
	
	public Monom(Monom m) {
		if (m != null) {
			this.grad = m.getGrad();
			this.valoare = m.getval();
		}
	}
	
	public int getGrad(){return this.grad;}
	public double getval(){return this.valoare;}
	public void setGrad(int grad){this.grad = grad;}
	public void setVal(double d){this.valoare = d;}
	public boolean isEmpty(){return ((new Float(valoare)).intValue() != 0 ) ? false : true;}
	
	public void inv() {valoare = -valoare;}
	public void add(Monom m1){valoare += m1.getval();}
	public void sub(Monom m1){valoare -= m1.getval();}
	public void mul(Monom m1){
		grad += m1.grad;
		valoare *= m1.valoare;
	}
	public void div(Monom m1) {
		grad -= m1.grad;
		valoare /=m1.valoare;
	}
	public void derive() {
		valoare *= grad;
		grad--;
	}
	public void integrate() {
		grad++;
		valoare /= grad;
	}
	public String toString() {
		String text = "";
		if (this.getval() == 0) return "";
		if (this.getval() > 0) {text+="+";}
		if (this.getval() == 1 && this.getGrad() > 0) {text+="X";}
		else if (this.getval() == -1 && this.getGrad() > 0) {text+="-X";}
		else if (this.getGrad() > 0) {text+=this.getval()+"X";}
		else {text+=this.getval();}
		text +=(this.getGrad() <= 1) ? "" : "^" + this.getGrad();
		return text;
	}
}
