package polynomial;

import java.util.regex.Pattern;
import javax.xml.soap.Text;

import java.util.HashMap;
import java.util.regex.Matcher;

public class Polinom {
	private int maxGrad;
	private HashMap<Integer, Monom> monomMap = new HashMap<Integer, Monom>();
	
	public Polinom() {
		maxGrad = 0;
	}
	
	public Polinom(String polyString) {
		double coef = 0;//. variable used to store the coeficient of an monomial
		int pow = 0;//. variable used to store the numeric value of the power of a monomial
		boolean signCoef = true;//. varialbe used to store the sign of the monomial coeficient: true => +, false => -
		int sequenceLength = 0;//. variable used to store the length of the found sequence
		String sequence = "";//. variable used to store the sequence found by the m.group() function
		String pattern = "[X0-9^.]+|[+-]";//. the pattern that helps us to find the monomials
		Pattern p = Pattern.compile(pattern); // variable that contains the compiled pattern 
		Matcher m = p.matcher(polyString); // a matcher object with the input string
	
		while(m.find()) {
			sequence = m.group();
			sequenceLength = sequence.length();
			if ( sequenceLength == 1 && (sequence.equals("+") || sequence.equals("-")) ) {
				if (sequence.equals("-"))
					signCoef = false;
				else 
					signCoef = true;
			} else {
				int indexOfX = 0;
				while (indexOfX < sequenceLength && sequence.charAt(indexOfX) != 'X') {indexOfX++;}
				if (indexOfX == sequenceLength) {
					coef = (signCoef) ? Double.parseDouble(sequence) : -Double.parseDouble(sequence);
					pow = 0;
				} else {
					coef = (indexOfX == 0) ? 1 : Double.parseDouble(sequence.substring(0, indexOfX));
					coef = (signCoef) ? coef : -coef;
					pow = (indexOfX == (sequenceLength - 1)) ? 1 : Integer.parseInt(sequence.substring(indexOfX + 2, sequenceLength));
				}
				maxGrad = (pow > maxGrad) ? pow : maxGrad;
				monomMap.put(Integer.valueOf(pow), new Monom(pow, coef));
			}
		}
	}
	public Polinom(Polinom p) {
		int pPow = p.getPower();
		Monom pMonom;
		while (pPow >= 0) {
			pMonom = (p.getMonom(pPow) != null) ? p.getMonom(pPow) : null;
			if (pMonom != null) {this.insert(pMonom);}
			pPow--;
		}	
	}
	public Polinom(Monom m) {this.insert(m);}
	
	public int getPower() {return maxGrad;}
	public Monom getMonom(int key) {return monomMap.get(Integer.valueOf(key));}
	public boolean isZero() {
		return (maxGrad == 0 && (this.getMonom(0) == null 
				|| this.getMonom(0).getval() == 0)) ? true : false;
	}
	
	public void insert(Monom m) {
		int monomPow = m.getGrad();
		if (monomMap.containsKey(Integer.valueOf(monomPow)))
			monomMap.get(monomPow).setVal(m.getval());
		else {
			monomMap.put(Integer.valueOf(monomPow), m);
			maxGrad = (monomPow >= maxGrad) ? monomPow : maxGrad;
		}
	}
	public String toString() {
		String text = "";
		int index = maxGrad;
		Monom object = monomMap.get(index);
		while(index >= 0) {
			if (object != null && object.getval() != 0) {text += object.toString();}
			index--;
			object = monomMap.get(index);
		}
		if (text.length() > 1)
			if (text.charAt(0) == '+') text = text.substring(1);
		if (text == "") text = "0";
		return text;
	}
}


