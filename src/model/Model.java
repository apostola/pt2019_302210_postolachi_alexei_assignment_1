package model;

import polynomial.*;

public class Model {
	private Polinom polinom1 = new Polinom();
	private Polinom polinom2 = new Polinom();
	private Polinom rezultat = new Polinom();
	private Operatii operatii = new Operatii();
			
	public Model() {
		this.reset();
	}
	
	public void reset() {
		polinom1 = new Polinom();
		polinom2 = new Polinom();
		rezultat = new Polinom();
	}
	
	public void readPolinom(String text, boolean polinom) {
		if (polinom) {polinom1 = new Polinom(text);}
		else {polinom2 = new Polinom(text);}
	}
	
	public String returnRezultat() {return rezultat.toString();}
	public String returnPolinom() {return polinom1.toString();}
	public String returnPolinom2() {return polinom2.toString();}
	public void add() {rezultat = operatii.add(polinom1, polinom2);}
	public void sub() {rezultat = operatii.sub(polinom1, polinom2);};
	public void mul() {rezultat = operatii.mult(polinom1, polinom2);}
	public void der() {rezultat = operatii.derive(polinom1);}
	public void integ() {rezultat = operatii.integrate(polinom1);}
}
